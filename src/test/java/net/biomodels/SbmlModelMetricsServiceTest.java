package net.biomodels;

import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;

import static org.junit.Assert.*;

public class SbmlModelMetricsServiceTest {

    @Test
    public void getStatsForNullModel() {
        assertNull(SbmlModelMetricsService.getStatsFor(null));
    }

    @Test
    public void getStatsForEmptyModel() {
        assertNull(SbmlModelMetricsService.getStatsFor(""));
    }

    @Test
    public void getStatsForFBCModel() {
        String modelPath = "src/test/resources/iAB_RBC_283.xml";
        assertTrue(SbmlReaderService.parse(modelPath).isPackageEnabled("fbc"));

        ModelStats stats = SbmlModelMetricsService.getStatsFor(modelPath);
        assertNotNull("the model statistics are not null", stats);
        assertEquals(0, stats.getMathElements());
        assertEquals(469, stats.getReactions());
        assertEquals(469, stats.getTotal());
    }

    @Test
    public void testReadDocumentNPE() {
        String modelPath = "src/test/resources/HTimmR.xml";
        SBMLDocument document = SbmlReaderService.parse(modelPath);
        assertNotNull(document);
    }
}
