package net.biomodels;

import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;

import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class App {
    public static final int NO_ARG_CODE = 1;
    public static final int NOT_A_LOCATION_CODE = 2;

    public static void main( String[] args ) throws IOException {
//        App.computeStatistics(args);
        System.out.println("Test Online SBML Validator");
        App.validateSBMLFile();
    }

    public static void validateSBMLFile() throws IOException {
        // the stream holding the file content

        String fileName = "data/Carcione2020.xml";
        if (fileName.isEmpty()) {
            throw new IOException();
        } else {
            try (InputStream inputStream = App.class.getClassLoader().getResourceAsStream(fileName)) {
                SBMLReader sbmlReader = new SBMLReader();
                SBMLDocument doc = sbmlReader.readSBMLFromStream(inputStream);
                boolean valid = SbmlReaderService.validateSBMLDocument(doc);
                System.out.println(valid ? "Model is valid" : "Model is invalid");
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
        }
    }

    public static void computeStatistics(String[] args) throws IOException {
        if (args.length < 2) {
            String msg = String.format(
                "Missing command file arguments [INPUT_FILE] [OUTPUT_FILE]%n" +
                "where%n" +
                "\tINPUT_FILE is a file containing the models to be processed (1 model per line)%n" +
                "\tOUTPUT_FILE is the location of the file where the stats should be written");
            exitWithError(msg, NO_ARG_CODE);
        }

        String modelQueueFileArg = args[0];
        Path modelQueuePath = Paths.get(modelQueueFileArg);
        if (!Files.isRegularFile(modelQueuePath)) {
            String msg = String.format("Location '%s' is not a file.%n", modelQueueFileArg);
            exitWithError(msg, NOT_A_LOCATION_CODE);
        }

        String outputFileArg = args[1];
        Path outputFile = Paths.get(outputFileArg);
        Path parentFolder = outputFile.normalize().getParent();
        if (null == parentFolder ||
                !(Files.isDirectory(parentFolder) && Files.isWritable(parentFolder))) {
            String msg = String.format("Location '%s' is not writable%n", parentFolder);
            exitWithError(msg, NOT_A_LOCATION_CODE);
        }

        List<String> modelQueue = Files.readAllLines(modelQueuePath, StandardCharsets.UTF_8);
        Set<ModelStats> modelStats = getModelStats(modelQueue);
        SimpleCsvFileWriter.write(outputFileArg, modelStats);
    }

    private static Set<ModelStats> getModelStats(List<String> modelQueue) {
        return modelQueue.parallelStream()
            .map(SbmlModelMetricsService::getStatsFor)
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
    }

    private static void exitWithError(String message, int statusCode) {
        System.err.println(message);
        System.exit(statusCode);
    }
}
